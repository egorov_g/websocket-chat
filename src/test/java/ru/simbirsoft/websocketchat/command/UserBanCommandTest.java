package ru.simbirsoft.websocketchat.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;
import ru.simbirsoft.websocketchat.util.DateUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserBanCommandTest {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;


    @Autowired
    private UserBanCommand banCommand;

    @Test
    public void ifHaveEnoughAuthorityTest() {
        String[] args = {"//user", "ban", "-l", "Dima", "-m", "15"};
        String username = "Pavel";
        String roomName = null;
        Date date = java.util.Date.from(LocalDateTime.now().plusMinutes(Integer.parseInt(args[5]))
                .atZone(ZoneId.systemDefault())
                .toInstant());
        String result;

        result = banCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("The user Dima is banned to " + DateUtils.formatDate(date) + ".", result);
        Assert.assertNotEquals("You don't have enough authority.", result);
        User user = findUser("Dima");
        user.getUnlockTimeSet().clear();
        userService.save(user);
    }

    @Test
    public void ifNotHaveEnoughAuthorityTest() {
        String[] args = {"//user", "ban", "-l", "Dima", "-m", "15"};
        String username = "Jenya";
        String roomName = null;
        Date date = java.util.Date.from(LocalDateTime.now().plusMinutes(Integer.parseInt(args[5]))
                .atZone(ZoneId.systemDefault())
                .toInstant());
        String result;

        result = banCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("You don't have enough authority.", result);
        Assert.assertNotEquals("The user Dima is banned to " + DateUtils.formatDate(date) + ".", result);

    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
