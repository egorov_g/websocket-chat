package ru.simbirsoft.websocketchat.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.repo.RoomRepo;
import ru.simbirsoft.websocketchat.repo.UserRepo;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.RoomServiceImpl;

import java.util.Locale;

@RunWith(MockitoJUnitRunner.class)
public class RoomConnectCommandTest {

    @Mock
    private RoomRepo roomRepo;
    @Mock
    private UserRepo userRepo;

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private RoomService roomService = new RoomServiceImpl();

    @Test
    public void ifRoomIsExistUnitTest() {
        String[] args = {"//room", "connect", "Room1"};
        String username = "Gleb";
        String result = null;
        User user = new User();
        user.setUsername(username);
        Room room = new Room();
        room.setName(args[2]);

        Mockito.when(roomService.findByName(args[2])).thenReturn(room);
        Mockito.when(messageSource.getMessage("room.connectSuccessfully", new Object[]{username, args[2]}, Locale.getDefault())).thenReturn("The user " + username + " has entered the room " + args[2] + ".");

        if (roomService.findByName(args[2]) != null) {
            room = roomService.findByName(args[2]);
            result = messageSource.getMessage("room.connectSuccessfully", new Object[]{username, args[2]}, Locale.getDefault());
            roomService.save(room);
        }

        Assert.assertEquals("The user Gleb has entered the room Room1.", result);
        Assert.assertNotEquals("Unable to enter the room. This room does not exist.", result);
    }
}

