package ru.simbirsoft.websocketchat.command;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;
import ru.simbirsoft.websocketchat.util.DateUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomDisconnectCommandTest {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomDisconnectCommand disconnectCommand;

    @Test
    public void disconnectFromCurrentRoomTest() {
        String result;
        String[] args = {"//room", "disconnect"};
        String roomName = "Room1";
        String username = "Vova";
        if (!roomService.findByName(roomName).getUsers().contains(findUser(username))) {
            roomService.findByName(roomName).getUsers().add(findUser(username));
        }

        result = disconnectCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("The user Vova left the room.", result);
        Assert.assertNotEquals("The user Gleb left the room.", result);
    }

    @Test
    public void disconnectWithBanUserTest() {
        String result;
        String[] args = {"//room", "disconnect", "-l", "Vova", "-m", "10"};
        String roomName = "Room1";
        String username = "Gleb";
        Date date = java.util.Date.from(LocalDateTime.now().plusMinutes(Integer.parseInt(args[5]))
                .atZone(ZoneId.systemDefault())
                .toInstant());
        if (!roomService.findByName(roomName).getUsers().contains(findUser(username))) {
            roomService.findByName(roomName).getUsers().add(findUser(username));
        }

        result = disconnectCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("The user Vova is banned to " + DateUtils.formatDate(date) + ".", result);
        Assert.assertNotEquals("The user Vova left the room.", result);
        User user = findUser("Vova");
        user.getUnlockTimeSet().clear();
        userService.save(user);
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

}
