package ru.simbirsoft.websocketchat.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomRenameCommandTest {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomRenameCommand renameCommand;

    @Test
    public void roomRenameIfEnoughAuthorityTest() {
        String[] args = {"//room", "rename", "Room2", "ExampleRoom"};
        String username = "Jenya";
        String roomName = null;
        String result;

        result = renameCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("You don't have enough authority.", result);
        Assert.assertNotEquals("Room renamed from Room2 to ExampleRoom.", result);
    }

    @Test
    public void renameRoomIfNotExistTest() {
        String[] args = {"//room", "remove", "Room5", "ExampleRoom"};
        String username = "Gleb";
        String roomName = null;
        String result;

        result = renameCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("Unable to rename room. This room does not exist.", result);
        Assert.assertNotEquals("Room renamed from Room5 to ExampleRoom.", result);
    }
}
