package ru.simbirsoft.websocketchat.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomRemoveCommandTest {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomRemoveCommand removeCommand;

    @Test
    public void removeRoomIfExistTest() {
        String[] args = {"//room", "remove", "Room4"};
        String username = "Gleb";
        String roomName = null;
        String result;
        if (roomService.findByName(args[2]) == null) {
            Room room = new Room();
            room.setName(args[2]);
            room.setOwner(findUser(username));
            roomService.save(room);
        }

        result = removeCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("Removed room with name Room4.", result);
        Assert.assertNotEquals("Unable to delete room. This room does not exist.", result);
    }

    @Test
    public void removeRoomIfNotExistTest() {
        String[] args = {"//room", "remove", "Room5"};
        String username = "Gleb";
        String roomName = null;
        String result;

        result = removeCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("Unable to delete room. This room does not exist.", result);
        Assert.assertNotEquals("Removed room with name Room5.", result);
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
