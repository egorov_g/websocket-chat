package ru.simbirsoft.websocketchat.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import ru.simbirsoft.websocketchat.entity.Role;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserModeratorCommandTest {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Autowired
    private UserModeratorCommand moderatorCommand;

    @Test
    public void ifHaveEnoughAuthorityAppointTest() {
        String[] args = {"//user", "moderator", "-n", "Dima"};
        String username = "Pavel";
        String roomName = null;
        String result;

        result = moderatorCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("User Dima appointed moderator.", result);
        Assert.assertNotEquals("You don't have enough authority.", result);
    }

    @Test
    public void ifHaveEnoughAuthorityDemoteTest() {
        String[] args = {"//user", "moderator", "-d", "Dima"};
        String username = "Pavel";
        String roomName = null;
        String result;
        User user = findUser("Dima");
        user.getRoles().add(Role.MODERATOR);
        userService.save(user);

        result = moderatorCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("The user Dima is demoted from moderators.", result);
        Assert.assertNotEquals("You don't have enough authority.", result);

    }

    @Test
    public void ifHaveNotEnoughAuthorityAppointTest() {
        String[] args = {"//user", "moderator", "-n", "Dima"};
        String username = "Jenya";
        String roomName = null;
        String result;

        result = moderatorCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("You don't have enough authority.", result);
        Assert.assertNotEquals("User Dima appointed moderator.", result);

    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
