package ru.simbirsoft.websocketchat.command;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomCreateCommandTest {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomCreateCommand createCommand;

    @Test
    public void ifRoomIsExistTest() {
        String result;
        String[] args = {"//room", "create", "Room2"};
        String username = "Gleb";
        String roomName = null;

        result = createCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("A room with this name already exists.", result);
        Assert.assertNotEquals("Created room with the name Room2.", result);

    }

    @Test
    public void ifRoomIsNotExistTest() {
        String result;
        String[] args = {"//room", "create", "Room3"};
        String username = "Gleb";
        String roomName = null;
        if (roomService.findByName("Room3") != null) {
            roomService.remove(roomService.findByName("Room3"));
        }

        result = createCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("Created room with the name Room3.", result);
        Assert.assertNotEquals("A room with this name already exists.", result);

    }

}
