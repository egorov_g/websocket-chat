package ru.simbirsoft.websocketchat.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRenameCommandTest {
    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Autowired
    private UserRenameCommand renameCommand;

    @Test
    public void ifHaveEnoughAuthorityTest() {
        String[] args = {"//user", "rename", "Dima", "Daemon"};
        String username = "Dima";
        String roomName = null;
        String result;

        result = renameCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("User changed name from Dima to Daemon.", result);
        Assert.assertNotEquals("You don't have enough authority.", result);
        User user = findUser("Daemon");
        user.setUsername("Dima");
        userService.save(user);
    }

    @Test
    public void ifHaveNotEnoughAuthorityTest() {
        String[] args = {"//user", "rename", "Dima", "Daemon"};
        String username = "Jenya";
        String roomName = null;
        String result;

        result = renameCommand.executeCommand(args, username, roomName);

        Assert.assertEquals("You don't have enough authority.", result);
        Assert.assertNotEquals("User changed name from Dima to Daemon.", result);

    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
