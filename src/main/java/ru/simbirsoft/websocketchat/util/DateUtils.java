package ru.simbirsoft.websocketchat.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtils {

    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("HH:mm:ss");

    private DateUtils() {
    }

    public static String formatDate(Date date) {
        return DATE_FORMATTER.format(date);
    }
}
