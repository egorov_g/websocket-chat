package ru.simbirsoft.websocketchat.util;

import ru.simbirsoft.websocketchat.entity.UnlockTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

public final class UnlockTimeUtils {

    private UnlockTimeUtils() {
    }

    public static Date findMaxTime(Set<UnlockTime> unlockTimeSet) {
        ArrayList<Date> temp = new ArrayList<>();
        for (UnlockTime ut : unlockTimeSet) {
            temp.add(ut.getTime());
        }
        Collections.sort(temp);
        return temp.get(temp.size() - 1);
    }
}
