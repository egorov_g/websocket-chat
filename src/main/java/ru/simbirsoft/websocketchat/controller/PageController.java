package ru.simbirsoft.websocketchat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

@Controller
public class PageController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String showChatPage(Authentication authentication, Model model) {
        User user = userService.findByUsername(authentication.getName()).orElseThrow(() -> new UsernameNotFoundException(authentication.getName()));
        model.addAttribute("rooms", roomService.findByUsers(user));
        model.addAttribute("ownerRooms", roomService.findByOwner(user));
        return "chat";
    }

}

