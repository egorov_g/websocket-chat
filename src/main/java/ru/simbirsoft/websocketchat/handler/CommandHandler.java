package ru.simbirsoft.websocketchat.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.command.*;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class CommandHandler {

    @Autowired
    private UserService userService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomConnectCommand roomConnectCommand;
    @Autowired
    private RoomCreateCommand roomCreateCommand;
    @Autowired
    private RoomDisconnectCommand roomDisconnectCommand;
    @Autowired
    private RoomRemoveCommand roomRemoveCommand;
    @Autowired
    private RoomRenameCommand roomRenameCommand;
    @Autowired
    private UserBanCommand userBanCommand;
    @Autowired
    private UserModeratorCommand userModeratorCommand;
    @Autowired
    private UserRenameCommand userRenameCommand;
    @Autowired
    private HelpCommand helpCommand;

    private Map<String, BasicCommand> commands = new HashMap<>();

    @PostConstruct
    public void commandMapInitialize() {
        commands.put("//room connect", roomConnectCommand);
        commands.put("//room create", roomCreateCommand);
        commands.put("//room disconnect", roomDisconnectCommand);
        commands.put("//room remove", roomRemoveCommand);
        commands.put("//room rename", roomRenameCommand);
        commands.put("//user ban", userBanCommand);
        commands.put("//user moderator", userModeratorCommand);
        commands.put("//user rename", userRenameCommand);
        commands.put("//help", helpCommand);
    }

    public String handleCommand(String command, String username, String roomName) throws IOException {
        String result;
        String[] args = command.split(" ");
        if (args.length == 1 && commands.get(args[0]) != null) {
            result = commands.get(args[0]).executeCommand(args, username, roomName);
        } else if (args.length > 1 && commands.get(args[0] + " " + args[1]) != null) {
            result = commands.get(args[0] + " " + args[1]).executeCommand(args, username, roomName);
        } else result = messageSource.getMessage("handler.incorrectCommand", new Object[]{}, Locale.getDefault());
        return result;
    }
}
