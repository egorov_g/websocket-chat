package ru.simbirsoft.websocketchat.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;
import ru.simbirsoft.websocketchat.util.DateUtils;
import ru.simbirsoft.websocketchat.util.UnlockTimeUtils;
import ru.simbirsoft.websocketchat.websocket.SimpleWebSocketHandler;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

@Component
public class RoomConnectCommand implements BasicCommand {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;
    @Autowired
    private SimpleWebSocketHandler simpleWebSocketHandler;

    @Override
    public String executeCommand(String[] args, String username, String roomName) throws IOException {
        String result = null;
        Date date = new Date();
        if (roomService.findByName(args[2]) != null) {
            Room room = roomService.findByName(args[2]);
            User user = findUser(username);
            if (!room.isPrivateRoom() || room.getUsers().size() < 2) {
                if (args.length == 3) {
                    if (user.getUnlockTimeSet() == null || user.getUnlockTimeSet().isEmpty() || UnlockTimeUtils.findMaxTime(user.getUnlockTimeSet()).before(date)) {
                        room.getUsers().add(user);
                        simpleWebSocketHandler.sendRoomMessages(username, roomName);
                        result = messageSource.getMessage("room.connectSuccessfully", new Object[]{username, args[2]}, Locale.getDefault());
                    } else
                        result = messageSource.getMessage("user.banInfo", new Object[]{DateUtils.formatDate(UnlockTimeUtils.findMaxTime(user.getUnlockTimeSet()))}, Locale.getDefault());
                } else if (args.length == 5 && args[3].equals("-l") && username.equals(room.getOwner().getUsername())) {
                    room.getUsers().add(findUser(args[4]));
                    simpleWebSocketHandler.sendRoomMessages(args[4], args[2]);
                    result = messageSource.getMessage("room.connectSuccessfully", new Object[]{args[4], args[2]}, Locale.getDefault());
                }
                roomService.save(room);
            } else
                result = messageSource.getMessage("room.connectPrivateRoomFailed", new Object[]{}, Locale.getDefault());
        } else result = messageSource.getMessage("room.connectFailed", new Object[]{}, Locale.getDefault());

        return result;
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
