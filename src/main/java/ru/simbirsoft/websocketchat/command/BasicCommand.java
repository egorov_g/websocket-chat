package ru.simbirsoft.websocketchat.command;

import java.io.IOException;

public interface BasicCommand {

    String executeCommand(String[] args, String username, String roomName) throws IOException;

}
