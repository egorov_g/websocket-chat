package ru.simbirsoft.websocketchat.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.entity.Role;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.UnlockTime;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;
import ru.simbirsoft.websocketchat.util.DateUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component
public class UserBanCommand implements BasicCommand {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Override
    public String executeCommand(String[] args, String username, String roomName) {
        String result = null;
        User user = findUser(args[3]);
        if (userService.findRoles(username).contains(Role.ADMIN) || userService.findRoles(username).contains(Role.MODERATOR)) {
            if (args[2].equals("-l") && args[4].equals("-m")) {
                List<Room> rooms = roomService.findByUsers(user);
                for (Room room : rooms) {
                    room.getUsers().remove(user);
                    roomService.save(room);
                }
                LocalDateTime localDateTime = LocalDateTime.now();
                Date date = java.util.Date.from(localDateTime.plusMinutes(Integer.parseInt(args[5]))
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
                UnlockTime ut = new UnlockTime();
                ut.setTime(date);
                user.addUnlockTime(ut);
                result = messageSource.getMessage("user.banSuccessfully", new Object[]{args[3], DateUtils.formatDate(date)}, Locale.getDefault());
            }
            userService.save(user);
        } else result = messageSource.getMessage("user.commandFailed", new Object[]{}, Locale.getDefault());
        return result;
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
