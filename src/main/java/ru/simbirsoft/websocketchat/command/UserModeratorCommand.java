package ru.simbirsoft.websocketchat.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.entity.Role;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.UserService;

import java.util.Locale;

@Component
public class UserModeratorCommand implements BasicCommand {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;

    @Override
    public String executeCommand(String[] args, String username, String roomName) {
        String result = null;
        if (userService.findRoles(username).contains(Role.ADMIN)) {
            User user = findUser(args[3]);
            if (args[2].equals("-n")) {
                user.getRoles().add(Role.MODERATOR);
                result = messageSource.getMessage("user.moderatorAppoint", new Object[]{args[3]}, Locale.getDefault());
            } else if (args[2].equals("-d")) {
                user.getRoles().remove(Role.MODERATOR);
                result = messageSource.getMessage("user.moderatorDemote", new Object[]{args[3]}, Locale.getDefault());
            }
            userService.save(user);
        } else result = messageSource.getMessage("user.commandFailed", new Object[]{}, Locale.getDefault());
        return result;
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
