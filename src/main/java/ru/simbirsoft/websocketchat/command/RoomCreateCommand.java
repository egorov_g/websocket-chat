package ru.simbirsoft.websocketchat.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

import java.util.Locale;

@Component
public class RoomCreateCommand implements BasicCommand {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Override
    public String executeCommand(String[] args, String username, String roomName) {
        String result;
        if (roomService.findByName(args[2]) == null) {
            Room room = new Room();
            room.setOwner(findUser(username));
            if (args.length == 3) {
                room.setName(args[2]);
            } else if (args.length == 4 && args[2].equals("-c")) {
                room.setPrivateRoom(true);
                room.setName(args[3]);
            }
            roomService.save(room);
            result = messageSource.getMessage("room.createSuccessfully", new Object[]{room.getName()}, Locale.getDefault());
        } else result = messageSource.getMessage("room.createFailed", new Object[]{}, Locale.getDefault());
        return result;
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
