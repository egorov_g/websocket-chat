package ru.simbirsoft.websocketchat.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.entity.Role;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.UserService;

import java.util.Locale;

@Component
public class UserRenameCommand implements BasicCommand {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;


    @Override
    public String executeCommand(String[] args, String username, String roomName) {
        String result;
        if (username.equals(args[2]) || userService.findRoles(username).contains(Role.ADMIN)) {
            User user = findUser(args[2]);
            user.setUsername(args[3]);
            userService.save(user);
            result = messageSource.getMessage("user.renameSuccessfully", new Object[]{args[2], args[3]}, Locale.getDefault());
        } else result = messageSource.getMessage("user.commandFailed", new Object[]{}, Locale.getDefault());
        return result;
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
