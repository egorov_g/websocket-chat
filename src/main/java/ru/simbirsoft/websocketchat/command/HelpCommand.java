package ru.simbirsoft.websocketchat.command;

import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class HelpCommand implements BasicCommand {

    private final String LIST_OF_COMMANDS = "Комнаты:\n//room create {Название комнаты} - создает комнаты\n" +
            "\t-c закрытая комната. Только (владелец, модератор и админ) может " +
            "добавлять/удалять пользоватей из комнаты\n\n" +
            "//room remove {Название комнаты} - удаляет комнату (владелец и админ)\n\n" +
            "//room rename {Название комнаты} - переименование комнаты (владелец и админ)\n\n" +
            "//room connect {Название комнаты} - войти в комнату\n" +
            "\t-l {login пользователя} - добавить пользователя в комнату\n\n" +
            "//room disconnect - выйти из текущей комнаты\n\n" +
            "//room disconnect {Название комнаты} - выйти из заданной комнаты\n\n" +
            "//room disconnect - выйти из текущей комнаты\n" +
            "\t-l {login пользователя} - выгоняет пользователя из комнаты (для владельца," +
            "модератора и админа)\n" +
            "\t-m {Колличество минут} - время на которое пользователь не сможет войти (для " +
            "владельца, модератора и админа)\n\n" +
            "Пользователи:\n" +
            "//user rename {login пользователя} (владелец и админ)\n\n" +
            "//user ban\n" +
            "\t-l {login пользователя} - выгоняет пользователя из всех комнат\n" +
            "\t-m {Колличество минут} - время на которое пользователь не сможет войти\n\n" +
            "//user moderator {login пользователя} - действия над модераторами\n" +
            "\t-n - назначить пользователя модератором\n" +
            "\t-d - разжаловать пользователя\n";

    @Override
    public String executeCommand(String[] args, String username, String roomName) throws IOException {
        return LIST_OF_COMMANDS;
    }
}
