package ru.simbirsoft.websocketchat.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.entity.Role;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

import java.util.Locale;

@Component
public class RoomRenameCommand implements BasicCommand {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Override
    public String executeCommand(String[] args, String username, String roomName) {
        String result;
        if (roomService.findByName(args[2]) != null) {
            Room room = roomService.findByName(args[2]);
            if (username.equals(room.getOwner().getUsername()) || userService.findRoles(username).contains(Role.ADMIN)) {
                room.setName(args[3]);
                roomService.save(room);
                result = messageSource.getMessage("room.renameSuccessfully", new Object[]{args[2], args[3]}, Locale.getDefault());
            } else
                result = messageSource.getMessage("room.renameNotEnoughAuthority", new Object[]{}, Locale.getDefault());
        } else result = messageSource.getMessage("room.renameFailed", new Object[]{}, Locale.getDefault());
        return result;
    }
}
