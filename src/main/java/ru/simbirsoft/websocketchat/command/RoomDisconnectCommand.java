package ru.simbirsoft.websocketchat.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.simbirsoft.websocketchat.entity.Role;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.UnlockTime;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;
import ru.simbirsoft.websocketchat.util.DateUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

@Component
public class RoomDisconnectCommand implements BasicCommand {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RoomService roomService;

    @Override
    public String executeCommand(String[] args, String username, String roomName) {
        String result = null;
        Room room = null;
        if (args.length == 2) {
            room = roomService.findByName(roomName);
            room.getUsers().remove(findUser(username));
            result = messageSource.getMessage("room.disconnect", new Object[]{username}, Locale.getDefault());
        } else if (args.length == 3) {
            room = roomService.findByName(args[2]);
            room.getUsers().remove(findUser(username));
            result = messageSource.getMessage("room.disconnect", new Object[]{username}, Locale.getDefault());
        } else if (args.length == 6 && args[2].equals("-l") && args[4].equals("-m") && (userService.findRoles(username).contains(Role.MODERATOR) || userService.findRoles(username).contains(Role.ADMIN) || findUser(username).equals(roomService.findByName(roomName).getOwner()))) {
            User user = findUser(args[3]);
            room = roomService.findByName(roomName);
            room.getUsers().remove(user);
            LocalDateTime localDateTime = LocalDateTime.now();
            Date date = java.util.Date.from(localDateTime.plusMinutes(Integer.parseInt(args[5]))
                    .atZone(ZoneId.systemDefault())
                    .toInstant());
            UnlockTime ut = new UnlockTime();
            ut.setTime(date);
            user.addUnlockTime(ut);
            userService.save(user);
            result = messageSource.getMessage("room.userBan", new Object[]{args[3], DateUtils.formatDate(date)}, Locale.getDefault());
        }
        roomService.save(room);
        return result;
    }

    private User findUser(String username) {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
