package ru.simbirsoft.websocketchat.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.simbirsoft.websocketchat.entity.User;

import java.util.Optional;

/**
 * Репозиторий пользователей
 */
public interface UserRepo extends JpaRepository<User, Long> {

    /**
     * Находит Optional пользователя по его имени
     *
     * @param username имя пользователя
     * @return Optional пользователя
     */
    Optional<User> findByUsername(String username);

    /**
     * Сохраняет пользователя в репозиторий
     *
     * @param user пользователь
     * @return пользователя
     */
    User save(User user);

}