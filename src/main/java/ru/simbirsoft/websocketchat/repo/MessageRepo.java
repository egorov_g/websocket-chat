package ru.simbirsoft.websocketchat.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.simbirsoft.websocketchat.entity.Message;

import java.util.List;

/**
 * Репозиторий сообщений
 */
public interface MessageRepo extends JpaRepository<Message, Long> {

    /**
     * Находит список сообщений по его автору
     *
     * @param author username автора
     * @return список сообщений
     */
    List<Message> findByAuthor(String author);

    /**
     * Находит все сообщения
     *
     * @return список сообщений
     */
    List<Message> findAll();

    /**
     * Находит сообщения по названию комнаты
     *
     * @param roomName название комнаты
     * @return список сообщений
     */
    List<Message> findByRoomName(String roomName);

    /**
     * Сохраняет сообщение в репозиторий
     *
     * @param message сообщение
     * @return сообщение
     */
    Message save(Message message);
}