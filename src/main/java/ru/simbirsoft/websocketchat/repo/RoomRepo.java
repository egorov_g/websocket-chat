package ru.simbirsoft.websocketchat.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;

import java.util.List;

/**
 * Репозиторий комнат
 */
public interface RoomRepo extends JpaRepository<Room, Long> {

    /**
     * Находит комнату по ее названию
     *
     * @param name название комнаты
     * @return комнату
     */
    Room findByName(String name);

    /**
     * Находит список комнат где пользователь является владельцем
     *
     * @param user пользователь
     * @return список комнат
     */
    List<Room> findByOwner(User user);

    /**
     * Находит список комнат где пользователь является гостем
     *
     * @param user пользователь
     * @return список комнат
     */

    List<Room> findByUsers(User user);

    /**
     * Сохраняет комнату в репозиторий
     *
     * @param room комната
     * @return комнату
     */
    Room save(Room room);

    /**
     * Удаляет комнату из репозитория
     * @param room комната
     */
    void delete(Room room);
}
