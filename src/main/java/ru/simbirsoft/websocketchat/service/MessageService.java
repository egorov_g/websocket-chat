package ru.simbirsoft.websocketchat.service;

import ru.simbirsoft.websocketchat.entity.Message;

import java.util.List;

/**
 * Сервис сообщений
 */
public interface MessageService {

    /**
     * Находит список сообщений по его автору
     * @param author username автора
     * @return список сообщений
     */
    List<Message> findByAuthor(String author);

    /**
     * Находит все сообщения
     * @return список сообщений
     */
    List<Message> findAll();

    /**
     * Находит сообщения по названию комнаты
     * @param roomName название комнаты
     * @return список сообщений
     */
    List<Message> findByRoomName (String roomName);

    /**
     * Сохраняет сообщение в репозиторий
     * @param message сообщение
     * @return сообщенеие
     */
    Message save(Message message);
}
