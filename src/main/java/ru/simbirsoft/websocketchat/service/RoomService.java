package ru.simbirsoft.websocketchat.service;

import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;

import java.util.List;

/**
 * Сервис комнат
 */
public interface RoomService {
    /**
     * Находит комнату по ее названию
     *
     * @param name название комнаты
     * @return комнату
     */
    Room findByName(String name);

    /**
     * Находит список комнат где пользователь являеется владельцем
     *
     * @param user полльзователь
     * @return список комнат
     */
    List<Room> findByOwner(User user);

    /**
     * Находит список комнат где пользователь является гостем
     *
     * @param user пользователь
     * @return список комнат
     */
    List<Room> findByUsers(User user);

    /**
     * Сохраняет комнату в репозиторий
     *
     * @param room комната
     * @return комнату
     */
    Room save(Room room);

    /**
     * Удаляет комнату из репозитория
     *
     * @param room комната
     */
    void remove(Room room);
}

