package ru.simbirsoft.websocketchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.simbirsoft.websocketchat.entity.Message;
import ru.simbirsoft.websocketchat.repo.MessageRepo;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepo messageRepo;

    @Override
    public List<Message> findAll() {
        return messageRepo.findAll();
    }

    @Override
    public List<Message> findByRoomName(String roomName) {
        return messageRepo.findByRoomName(roomName);
    }

    @Override
    public List<Message> findByAuthor(String author) {
        return messageRepo.findByAuthor(author);
    }

    @Override
    public Message save(Message message) {
        return messageRepo.save(message);
    }
}
