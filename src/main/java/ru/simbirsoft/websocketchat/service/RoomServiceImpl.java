package ru.simbirsoft.websocketchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.repo.RoomRepo;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepo roomRepo;

    @Override
    public Room findByName(String name) {
        return roomRepo.findByName(name);
    }

    @Override
    public List<Room> findByOwner(User user) {
        return roomRepo.findByOwner(user);
    }

    @Override
    public List<Room> findByUsers(User user) {
        return roomRepo.findByUsers(user);
    }

    @Override
    public Room save(Room room) {
        return roomRepo.save(room);
    }

    @Override
    public void remove(Room room) {
        roomRepo.delete(room);
    }
}
