package ru.simbirsoft.websocketchat.service;

import ru.simbirsoft.websocketchat.entity.Role;
import ru.simbirsoft.websocketchat.entity.User;

import java.util.Optional;
import java.util.Set;

public interface UserService {

    /**
     * Находит пользователя по его имени
     *
     * @param username имя пользователя
     * @return пользователя
     */
    Optional<User> findByUsername(String username);

    /**
     * Сохраняет пользователя в репозиторий
     *
     * @param user пользователь
     * @return пользователя
     */
    User save(User user);


    /**
     * Находит список ролей пользователя
     *
     * @param username имя пользователя
     * @return пользователя
     */
    Set<Role> findRoles(String username);
}
