package ru.simbirsoft.websocketchat.entity;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    USER, MODERATOR, ADMIN, BLOCKED_USER;

    @Override
    public String getAuthority() {
        return name();
    }
}