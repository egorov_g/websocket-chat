package ru.simbirsoft.websocketchat.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;
import ru.simbirsoft.websocketchat.entity.Message;
import ru.simbirsoft.websocketchat.entity.Room;
import ru.simbirsoft.websocketchat.entity.User;
import ru.simbirsoft.websocketchat.handler.CommandHandler;
import ru.simbirsoft.websocketchat.model.InputMessageDTO;
import ru.simbirsoft.websocketchat.model.OutputMessageDTO;
import ru.simbirsoft.websocketchat.service.MessageService;
import ru.simbirsoft.websocketchat.service.RoomService;
import ru.simbirsoft.websocketchat.service.UserService;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@Component
public class SimpleWebSocketHandler implements WebSocketHandler {

    private static final String COMMON_ROOM_NAME = "Common";

    @Autowired
    private CommandHandler commandHandler;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private MessageSource messageSource;

    private Set<WebSocketSession> sessions = new HashSet<>();
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        Message message = createJoinedMessage(webSocketSession);
        sessions.add(webSocketSession);
        Room room = roomService.findByName(COMMON_ROOM_NAME);
        room.getUsers().add(findUser(webSocketSession));
        roomService.save(room);
        if (!messageService.findAll().isEmpty()) {
            for (Message msg : messageService.findAll()) {
                if (isRoomContainsUser(webSocketSession, msg)) {
                    webSocketSession.sendMessage(createTextMessage(msg));
                }
            }
        }
        for (WebSocketSession session : sessions) {
            if (isRoomContainsUser(session, message)) {
                session.sendMessage(createTextMessage(message));
            }
        }
        messageService.save(message);
    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        String result;
        InputMessageDTO inputMessageDTO = mapper.readValue(webSocketMessage.getPayload().toString(), InputMessageDTO.class);
        if (inputMessageDTO.getPayload().indexOf("//") == 0) {
            result = commandHandler.handleCommand(inputMessageDTO.getPayload(), webSocketSession.getPrincipal().getName(), inputMessageDTO.getRoom());
            webSocketSession.sendMessage(createTextMessage(createMessage(webSocketSession, new InputMessageDTO(inputMessageDTO.getRoom(), result))));
        } else {
            Message message = createMessage(webSocketSession, inputMessageDTO);
            for (WebSocketSession session : sessions) {
                if (isRoomContainsUser(session, message)) {
                    session.sendMessage(createTextMessage(message));
                }
            }
            messageService.save(message);
        }
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
        Room room = roomService.findByName(COMMON_ROOM_NAME);
        room.getUsers().remove(findUser(webSocketSession));
        roomService.save(room);
        Message message = createLeftMessage(webSocketSession);
        sessions.remove(webSocketSession);
        for (WebSocketSession session : sessions) {
            if (isRoomContainsUser(session, message)) {
                session.sendMessage(createTextMessage(message));
            }
        }
        messageService.save(message);
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    private TextMessage createTextMessage(Message message) throws JsonProcessingException {
        String jsonStr = mapper.writeValueAsString(new OutputMessageDTO(message));
        return new TextMessage(jsonStr);
    }

    private Message createJoinedMessage(WebSocketSession webSocketSession) {
        return new Message(new Date(), findUser(webSocketSession), COMMON_ROOM_NAME, messageSource.getMessage("user.singedIn", new Object[]{}, Locale.getDefault()));
    }

    private Message createLeftMessage(WebSocketSession webSocketSession) {
        return new Message(new Date(), findUser(webSocketSession), COMMON_ROOM_NAME, messageSource.getMessage("user.logout", new Object[]{}, Locale.getDefault()));
    }

    private Message createMessage(WebSocketSession webSocketSession, InputMessageDTO inputMessageDTO) {
        return new Message(new Date(), findUser(webSocketSession), inputMessageDTO.getRoom(), inputMessageDTO.getPayload());
    }

    private User findUser(WebSocketSession webSocketSession) {
        String username = webSocketSession.getPrincipal().getName();
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

    private boolean isRoomContainsUser(WebSocketSession webSocketSession, Message message) {
        return roomService.findByName(message.getRoomName()).getUsers().contains(findUser(webSocketSession)) || roomService.findByName(message.getRoomName()).getOwner().getUsername().equals(findUser(webSocketSession).getUsername());
    }

    public void sendRoomMessages(String username, String roomName) throws IOException {
        for (WebSocketSession webSocketSession : sessions) {
            if (webSocketSession.getPrincipal().getName().equals(username)) {
                if (!messageService.findByRoomName(roomName).isEmpty()) {
                    for (Message msg : messageService.findByRoomName(roomName)) {
                        webSocketSession.sendMessage(createTextMessage(msg));
                    }
                }
            }
        }
    }
}
