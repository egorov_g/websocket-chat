package ru.simbirsoft.websocketchat.model;

public class InputMessageDTO {
    private String room;
    private String payload;

    public InputMessageDTO() {
    }

    public InputMessageDTO(String room, String payload) {
        this.room = room;
        this.payload = payload;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
