package ru.simbirsoft.websocketchat.model;

import ru.simbirsoft.websocketchat.entity.Message;
import ru.simbirsoft.websocketchat.util.DateUtils;

public class OutputMessageDTO {
    private String room;
    private String from;
    private String payload;
    private String date;

    public OutputMessageDTO(Message message) {
        this.room = message.getRoomName();
        this.from = message.getAuthor().getUsername();
        this.payload = message.getPayload();
        this.date = DateUtils.formatDate(message.getCreateDate());
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
