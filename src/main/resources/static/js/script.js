var sock = new SockJS('http://localhost:8080/chat');
var currentRoom;

sock.onopen = function () {
	console.log('open');
    currentRoom = 'Common'
    hideAllRooms();
    showCurrentRoom();
};

sock.onmessage = function (e) {
	console.log('message', e.data);
	var message = JSON.parse(e.data);
	addMessage(message);
};

sock.onclose = function () {
	console.log('close');
};

function send() {
	var message = document.getElementById('message').value;
	var room = currentRoom;
	var output = {"room" : room, "payload" : message};
	sock.send(JSON.stringify(output));
	document.getElementById('message').value = "";
};

function addMessage (message) {
   var room = message.room;
   var area = document.getElementById(room);
   area.textContent = area.textContent + message.date + ':' + message.from + ':' + message.payload + '\n';
}

function joinRoom(room){
   hideCurrentRoom();
   currentRoom = room;
   showCurrentRoom();
}

function hideAllRooms(){
    var elements = document.getElementsByName('area');
        for (var i = 0; i < elements.length; i++) {
            var area = elements[i];
            area.style.display = 'none';
            }
}

function showCurrentRoom(){
    document.getElementById(currentRoom).style.display = '';
}

function hideCurrentRoom(){
    document.getElementById(currentRoom).style.display = 'none';
}
