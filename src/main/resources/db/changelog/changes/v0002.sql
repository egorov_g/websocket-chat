insert into usr (id, username, password)
    values (1, 'Gleb', '$2a$11$nLNkcrbiURZtWG.sZ8eflumyrttbCfwE5ptlHVMEMLDy9LnLdfVCW');

insert into usr (id, username, password)
    values (2, 'Pavel', '$2a$11$nLNkcrbiURZtWG.sZ8eflumyrttbCfwE5ptlHVMEMLDy9LnLdfVCW');

insert into usr (id, username, password)
    values (3, 'Dima', '$2a$11$nLNkcrbiURZtWG.sZ8eflumyrttbCfwE5ptlHVMEMLDy9LnLdfVCW');

insert into usr (id, username, password)
    values (4, 'Vova', '$2a$11$nLNkcrbiURZtWG.sZ8eflumyrttbCfwE5ptlHVMEMLDy9LnLdfVCW');

insert into usr (id, username, password)
    values (5, 'Jenya', '$2a$11$nLNkcrbiURZtWG.sZ8eflumyrttbCfwE5ptlHVMEMLDy9LnLdfVCW');

insert into user_role (user_id, role)
    values (1, 'USER'), (1, 'ADMIN');

insert into user_role (user_id, role)
    values (2, 'USER'), (2, 'ADMIN');

insert into user_role (user_id, role)
    values (3, 'USER');

insert into user_role (user_id, role)
    values (4, 'USER');

insert into user_role (user_id, role)
    values (5, 'USER');

insert into room (room_id, name, private_room, owner_id)
    values (1, 'Common', false, 1);

insert into room (room_id, name, private_room, owner_id)
    values (2, 'Room1', false, 1);

insert into room (room_id, name, private_room, owner_id)
    values (3, 'Room2', false, 1);
