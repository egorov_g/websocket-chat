create table user_role (
    user_id int8 not null,
    role varchar(255)
);
create table usr (
    id int8 not null,
    password varchar(255) not null,
    username varchar(255) not null,
    primary key (id)
);

create table msg (
    id int8 not null,
    author_id int8,
    room_name varchar(255),
    payload text,
    create_date timestamp,
    primary key (id)
 );
create table room (
    room_id int8 not null,
    name varchar(255),
    private_room boolean,
    owner_id int8,
    primary key (room_id));

create table users_in_room (
    room_id int8 not null,
    user_id int8 not null,
    primary key (room_id, user_id)
);

create table unlock_time (
    id int8 not null,
    time timestamp,
    user_id int8,
    primary key (id));

alter table if exists user_role
    add constraint user_id_fk
    foreign key (user_id) references usr;

alter table if exists msg
    add constraint author_id_fk
     foreign key (author_id) references usr;

alter table if exists room
    add constraint owner_id_fk
     foreign key (owner_id) references usr;

alter table if exists users_in_room
    add constraint room_id_fk
     foreign key (room_id) references room;

alter table if exists users_in_room
    add constraint users_id_fk
     foreign key (user_id) references usr;

alter table if exists unlock_time
    add constraint user_id_fk
    foreign key (user_id) references usr;
